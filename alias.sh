alias l="ls -lhX --group-directories-first --color=auto"
alias ll="l -a"
alias lll="ll"
alias c="clear"
alias h="cd"
alias ch="c && h"
alias hc="ch"
alias x="exit"
alias suod="sudo"
alias hg='history | grep $1'
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."

cl() {
	local dir="$1"
	local dir="${dir:=$HOME}"
	if [[ -d "$dir" ]]; then
		cd "$dir" >/dev/null; l
	else
		echo "bash: cdls: $dir: Directory not found"
	fi
}

export HISTIGNORE="l:ls:ll:lll:cd:c:h:pwd:exit:clear:x:ch:hc:hg:cl"
export HISTFILESIZE=-1
export HISTSIZE=-1
export HISTCONTROL=ignoredups:erasedups
export HISTTIMEFORMAT="[$(tput setaf 2)%F %T$(tput sgr0)]: " # colorful date


# shuf -n 1 ~/file.txt # Custom welcome message
